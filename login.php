<?php
if($logined){
    if($_SESSION['admin']=="Y") {
        ?>
        <script>window.location = '../../projectclass/manage/';</script>
    <?php
    } else {
        ?>
        <script>window.location = '../../projectclass/';</script>
            <?php
    }
    
    exit();
}
?>
<div class="row justify-content-center">

        <div class="col-md-3">
            <form id="form_login">
            <div class="card">
                <div class="card-header text-center bg-info">
                    เข้าสู่ระบบ
                </div>
                <div class="card-body">
                        <div class="form-group">
                                <label for="usr">ชื่อผู้ใข้:</label>
                                <input type="text" class="form-control" id="usr" name="usr" placeholder="ชื่อผู้ใข้" required >
                        </div>
                        <div class="form-group">
                                <label for="pwd">รหัสผ่าน:</label>
                                <input type="password" class="form-control" id="pwd" name="pwd" placeholder="รหัสผ่าน" required >
                        </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary" type="submit" >เข้าระบบ</button>
                    <span id="show_error" class="text-danger"> </span>
    
                </div>
            </div> <!-- card -->
            </form>

            <div class="card mt-3">
                <div class="card-body p-0">
                    <div class="list-group">
                        <a href="?page=register" class="list-group-item list-group-item-action"> <i class="fa fa-user-plus text-info"></i> สมัครสมาชิก</a>
                    </div>
                </div>
            </div>
            
        </div>

    </div> <!-- row -->

<script>
$('#form_login').submit(function(event){
    event.preventDefault();
    
    var data = $( "#form_login" ).serialize();
    
    $.post("login_check.php",data,function(info){
        //alert(info);
        if(info=="ok") {
            window.location = '?page=main';
        } else {
            $('#show_error').html(info);
        }
        
    });
});
</script>