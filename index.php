<?php
session_start();
include './includes/function.php';

if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = "main";
}


if (isset($_SESSION['user_id'])) {
    $logined = true;

    if($_SESSION['user_type_id']=='1'){

        // if($page != "profile") {
        //     header('location: ../../projectclass/teacher/');
        // }
        
    }

} else {

    // if($page != 'register') {
    //     $page = 'login';
    // }
   //  $page = 'main';

   $logined = false;
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ระบบริหารจัดการรายวิชาโครงงาน</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="./node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="./css/style.css">
    <link href="./css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="./node_modules/@fortawesome/fontawesome-free/css/all.css">
    <script src="./node_modules/jquery/dist/jquery.min.js"></script>
    <script src="./node_modules/popper.js/dist/umd/popper.min.js"></script>


</head>
<body>
<?php include './includes/header.php'; ?>

<div class="container-fluid ">
<div class="row">
<div class="col-12 mb-4">
<?php 
    include "./includes/carousel.php"; 
?>
</div>
</div>
<div class="col-12 mb-4">
<?php 
    include $page.".php"; 
?>
</div>
</div> <!-- container -->
<?php include './includes/footer.php'; ?>


    <script src="./node_modules/jquery/dist/jquery.min.js"></script>
    <script src="./node_modules/popper.js/dist/umd/popper.min.js"></script>
</body>
</html>