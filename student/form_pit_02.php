<div class="jumbotron">
  <form action="">
  <div class="card">
  <div class="card-header text-center">แบบคำร้องขอส่งเอกสารและขออนุมัติสอบหัวข้อ</div>
  <div class="card-body">
     <div class="form-group row">
            <label class="col-3" > วันที่กรอกใบคำร้อง</label>
                 <div class="col-8">
                 <input type="datetime-local" class="form-control" id="ddmmyy" name="ddmmyy" placeholder="ชื่อภาษาอังกฤษ" required >
            </div>
        </diV>
     <div class="form-group row">
            <label class="col-3" > ชื่อผู้ร่วมทำโครงงานคนที่ 1:</label>
                 <div class="col-8">
                 <input type="text" class="form-control" id="author1" name="author1" placeholder="ชื่อผู้ทำ" required >
                 <input type="text" class="form-control" id="code1" name="code1" placeholder="รหัสนักศึกษา" required >
            </div>
        </diV>
        <div class="form-group row">
            <label class="col-3" > ชื่อผู้ร่วมทำโครงงานคนที่ 2:</label>
                 <div class="col-8">
                 <input type="text" class="form-control" id="author2" name="author2" placeholder="ชื่อผู้ทำ"  >
                 <input type="text" class="form-control" id="code2" name="code2" placeholder="รหัสนักศึกษา" >
            </div>
        </diV>
        <div class="form-group row">
            <label class="col-3" > ชื่อโครงงานภาษาไทย:</label>
                 <div class="col-8">
                 <input type="text" class="form-control" id="thai_name" name="thai_name" placeholder="ชื่อภาษาไทย" required >
            </div>
        </diV>
        <div class="form-group row">
            <label class="col-3" > ชื่อโครงงานภาษาอังกฤษ:</label>
                 <div class="col-8">
                 <input type="text" class="form-control" id="eng_name" name="eng_name" placeholder="ชื่อภาษาอังกฤษ" required >
            </div>
        </diV>
        <div class="form-group row">
            <label class="col-3" > วันที่/เดือน/ปี เข้าสอบหัวข้อโครงงาน</label>
                 <div class="col-8">
                 <input type="datetime-local" class="form-control" id="ddmmyy" name="ddmmyy" placeholder="ชื่อภาษาอังกฤษ" required >
            </div>
        </diV>
        <div class="form-group row">
            <label class="col-3" > ลงชื่อนักศึกษา 1 :</label>
                 <div class="col-8">
                 <input type="text" class="form-control" id="signature1" name="signature1" placeholder="ลงชื่อผู้จัดทำคนที่ 1" required >
                 <input type="text" class="form-control" id="signature2" name="signature2" placeholder="ลงชื่อผู้จัดทำคนที่ 2" required >
            </div>
        </diV>
        </div> 
            <div class="card-footer"> 
                 <button type="submit" class="btn btn-primary  fa-pull-right">ยกเลิก</button> 
                 <button type="submit" class="btn btn-primary  fa-pull-right">บันทึก</button>
    </div>
</div>
  
       
       
  
  </form>
</div>

