<div class="jumbotron">
    <h3 class="text-center">ตารางยื่นคำร้องโครงงาน 1</h3>
<table class="table table-bordered table-striped table-hover mt-3">
    <thead class="bg-info ">
      <tr>
        <th>ลำดับ</th>
        <th>ชื่อคำร้อง</th>
        <th>สถานะการอนุมัติที่ปรึกษาหลัก</th>
        <th>สถานะการอนุมัติที่ปรึกษาร่วม</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td>สร้างหัวข้อโครงงาน</td>
        <td><p class="text-warning">รอการอนุมัติ</p><p class="text-success">อนุมัติแล้ว</p></td>
        <td><p class="text-warning">รอการอนุมัติ</p><p class="text-success">อนุมัติแล้ว</p></td>
       
      </tr>
      
    </tbody>
  </table>
  </div>