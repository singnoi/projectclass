<div class="jumbotron my-5">
    <form action="">
            <div class="card">
<!-- card -->
        <div class="card-header text-center">ใบเสนอหัวข้อโครงงาน</div>
        <div class="card-body">
        <div class="form-group row">
            <label class="col-3" > ชื่อโครงงานภาษาไทย:</label>
                 <div class="col-8">
                 <input type="text" class="form-control" id="thai_name" name="thai_name" placeholder="ชื่อภาษาไทย" required >
            </div>
        </diV>
        <div class="form-group row">
            <label class="col-3" > ชื่อโครงงานภาษาอังกฤษ:</label>
                 <div class="col-8">
                 <input type="text" class="form-control" id="eng_name" name="eng_name" placeholder="ชื่อภาษาอังกฤษ" required >
            </div>
        </diV>
        <div class="form-group row">
            <label class="col-3" > ชื่อผู้ร่วมทำโครงงานคนที่ 1:</label>
                 <div class="col-8">
                 <input type="text" class="form-control" id="author1" name="author1" placeholder="ชื่อผู้ทำ" required >
                 <input type="text" class="form-control" id="code1" name="code1" placeholder="รหัสนักศึกษา" required >
            </div>
        </diV>
        <div class="form-group row">
            <label class="col-3" > ชื่อผู้ร่วมทำโครงงานคนที่ 2:</label>
                 <div class="col-8">
                 <input type="text" class="form-control" id="author2" name="author2" placeholder="ชื่อผู้ทำ"  >
                 <input type="text" class="form-control" id="code2" name="code2" placeholder="รหัสนักศึกษา" >
            </div>
        </diV>
        <div class="form-group row">
            <label class="col-3" > อาจารย์ที่ปรึกษาหลักโครงงาน:</label>
                 <div class="col-8">
                 <input type="text" class="form-control" id="consultants1" name="consultants1" placeholder="ชื่อที่ปรึกษาหลัก" required >
            </div>
        </diV>
        <div class="form-group row">
            <label class="col-3" > อาจารย์ที่ปรึกษาร่วมโครงงาน:</label>
                 <div class="col-8">
                 <input type="text" class="form-control" id="consultants2" name="consultants2" placeholder="ชื่อที่ปรึกษาร่วม" required >
            </div>
        </diV>
        <div class="form-group row">
            <label class="col-3" > คำสำคัญของโครงงาน (Key words):</label>
                 <div class="col-8">
                 <textarea class="form-control" rows="5" id="keywords" name="keywords" placeholder="รายละเอียด..."></textarea>
            </div>
        </diV>
        <div class="form-group row">
            <label class="col-3" > ความสำคัญและที่มาของปัญหาที่ทำโครงงาน:</label>
                 <div class="col-8">
                 <textarea class="form-control" rows="5" id="important" name="important" placeholder="รายละเอียด..."></textarea>
            </div>
        </diV>
        <div class="form-group row">
            <label class="col-3" > วัตถุประสงค์ของโครงงาน:</label>
                 <div class="col-8">
                 <textarea class="form-control" rows="5" id="objective" name="objective" placeholder="รายละเอียด..."></textarea>
            </div>
        </diV>
        <div class="form-group row">
            <label class="col-3" > ขอบเขตของโครงงาน:</label>
                 <div class="col-8">
                 <textarea class="form-control" rows="5" id="scope" name="scope"placeholder="รายละเอียด..."></textarea>
            </div>
        </diV>
        <div class="form-group row">
            <label class="col-3" > ประโยชน์ที่คาดว่าจะได้จากโครงงาน:</label>
                 <div class="col-8">
                 <textarea class="form-control" rows="5" id="benefit" name="benefit" placeholder="รายละเอียด..."></textarea>
            </div>
        </diV>
        <div class="form-group row">
            <label class="col-3" > แผนการดำเนินงาน และขั้นตอนการดำเนินงาน:</label>
                 <div class="col-8">
                 <textarea class="form-control" rows="5" id="plan" name="plan" placeholder="รายละเอียด..."></textarea>
            </div>
        </diV>
        
        </div> 
        <div class="card-footer">
            <button type="submit" class="btn btn-primary fa-pull-right">บันทึก</button> 
            <button type="button" class="btn btn-secondary fa-pull-right">ยกเลิก</button>
        </div>
            
        </div>
       
        
        
    </form>
</div>