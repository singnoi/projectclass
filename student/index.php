<?php
session_start();
if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = "main";
}

if (isset($_SESSION['user_id'])) {
    $logined = true;
    // if($_SESSION['admin'] == 'N'){
    //     header('location: ../../projectclass/');
    //     exit();
    // }
} else {
    $logined = false;
    header('location: ../../projectclass/');
    exit();
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ระบบริหารจัดการรายวิชาโครงงาน</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="../node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/style.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link href="../css/all.css" rel="stylesheet">
    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/popper.js/dist/umd/popper.min.js"></script>
</head>
<body>
<?php include '../includes/header.php'; ?>
<div class="container">
<?php include $page.".php"; ?>
</div> <!-- container -->
<?php include '../includes/footer.php'; ?>
</body>
</html>