<?php 
if(isset($_GET['user_type_id'])) {
    $user_type_id = $_GET['user_type_id'];
} else {
    $user_type_id = '2';
}
?>
    <div class="row justify-content-center">

        <div class="col-md-6">
            <form id="form_reg">
                <input type="hidden" name="user_type_id" value="<?php echo $user_type_id;?>" >
            <div class="card">
                <div class="card-header">
                    เพิ่มสมาชิกเข้าระบบ ( <?php echo show_type_user($user_type_id);?> )
                </div>
                <div class="card-body">

                        <div class="form-group row">
                                <label class="col-sm-3 col-form-label">ชื่อเข้าใข้:</label>
                                <div class="col-sm-9">
                                <input type="text" class="form-control" id="usr" name="usr" placeholder="username" required>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-3 col-form-label">กำหนดรหัสผ่าน:</label>
                                <div class="col-sm-9">
                                <input type="password" class="form-control" id="pwd" name="pwd" placeholder="password" required>
</div>
                        </div>
                        <div class="form-group row">
                                <label class="col-sm-3 col-form-label">ยืนยันรหัสผ่าน:</label>
                                <div class="col-sm-9">
                                <input type="password" class="form-control" id="pwd2" name="pwd2" placeholder="password" required>
</div>
                        </div>

                        <div class="form-group row">
                                <label class="col-sm-3 col-form-label">คำนำหน้า:</label>
                                <div class="col-sm-9">

                                <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value="นาย" checked name="tname">นาย
                                </label>
                                </div>
                                <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value="นาง" name="tname">นาง
                                </label>
                                </div>
                                <div class="form-check-inline disabled">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value="นางสาว" name="tname">นางสาว
                                </label>
                                </div>

                                </div>
                        </div>

                        <div class="form-group row">
                                <label class="col-sm-3 col-form-label">ชื่อจริง:</label>
                                <div class="col-sm-9">
                                <input type="text" class="form-control" id="fname" name="fname" placeholder="ชื่อ" required>
                                </div>
                        </div>

                        <div class="form-group row">
                                <label class="col-sm-3 col-form-label">นามสกุล:</label>
                                <div class="col-sm-9">
                                <input type="text" class="form-control" id="lname" name="lname" placeholder="นามสกุล" required>
                                </div>
                        </div>

                        <div class="form-group row">
                                <label class="col-sm-3 col-form-label">รหัสประจำตัว:</label>
                                <div class="col-sm-9">
                                <input type="text" class="form-control" id="user_code" name="user_code" placeholder="606xxxxxxx" required>
                                </div>
                        </div>

                        <div class="form-group row">
                                <label class="col-sm-3 col-form-label">เบอร์โทร:</label>
                                <div class="col-sm-9">
                                <input type="text" class="form-control" id="user_tel" name="user_tel" placeholder="09xxxxxxx" required>
                                </div>
                        </div>

                        <div class="form-group row">
                                <label class="col-sm-3 col-form-label">อีเมล์:</label>
                                <div class="col-sm-9">
                                <input type="email" class="form-control" id="user_email" name="user_email" placeholder="123@aaa.bb" required>
                                </div>
                        </div>

           
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary" type="submit" >เก็บข้อมูล</button>
                    <span id="show_save" class="text-danger"></span>
                </div>
            </div> <!-- card -->
            </form>
            
        </div>

    </div> <!-- row -->
<script>
$('#form_reg').submit(function(e){
    e.preventDefault();
    var data = $('#form_reg').serialize();
    $.post("register_save.php",data,function(info){
        //alert(info);
        var obj = jQuery.parseJSON(info);
        if(obj.save =='ok'){
            alert("สมัครสมาชิกสำเร็จ สามารถลงชื่อเข้าใช้ระบบได้");
            if(obj.profile == 'N' && obj.user_type_id=='1') {
                window.location = '../../projectclass/manage/?page=main';
            } else if(obj.profile == 'N' && obj.user_type_id=='2') {
                window.location = '../../projectclass/manage/?page=student_list';
            } else {
                window.location = '?page=login';
            }
            
        } else {
            $('#show_save').html(obj.save);
        }
    });
});
</script>