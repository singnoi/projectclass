<div class="card">
            <div class="card-body p-0">
                <div class="list-group">
                    <a href="manage/?page=main" class="list-group-item list-group-item-action bg-info"> <i class="fa fa-home mr-3" ></i>  จัดการข้อมูลระบบ </a>
                    <a href="manage/" class="list-group-item list-group-item-action"> <i class="fas fa-user-tie mr-3"></i>ข้อมูลอาจารย์</a>
                    <a href="manage/?page=student_list" class="list-group-item list-group-item-action"> <i class="fas fa-user-graduate mr-3"></i> ข้อมูลนักศึกษา</a>
                    <a href="manage/?page=student_reg" class="list-group-item list-group-item-action"> <i class="fas fa-user-graduate mr-3"></i>รายการรออนุมัติเพี่อเข้าใช้งาน</a>
                    <a href="manage/?page=year_form" class="list-group-item list-group-item-action"> <i class="far fa-calendar-alt mr-3"></i>ปีการศึกษา</a>
                    <a href="manage/?page=subject_form" class="list-group-item list-group-item-action"> <i class="fas fa-server mr-3"></i>รายวิชาโครงการ</a>
                    
                    
                </div>
            </div>
        </div>