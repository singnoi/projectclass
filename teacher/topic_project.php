<div class="jumbotron">
<div class="container">
  <h2>รายชื่อหัวข้อโครงงาน</h2>      
  <table class="table table-bordered table-striped table-hover">
    <thead class="bg-info">
      <tr>
        <th>ลำดับที่</th>
        <th>ชื่อโครงงาน</th>
        <th>ชื่อผู้จัดทำ</th>
        <th>ภาคการเรียน</th>
        <th>ปีการศึกษา</th>
        <th>อาจารย์ที่ปรึกษาหลัก</th>
        <th>อาจารย์ที่ปรึกษาร่วม</th>
        <th>สถานะ</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>John</td>
        <td>Doe</td>
        <td>john@example.com</td>
      </tr>
      <tr>
        <td>Mary</td>
        <td>Moe</td>
        <td>mary@example.com</td>
      </tr>
      <tr>
        <td>July</td>
        <td>Dooley</td>
        <td>july@example.com</td>
      </tr>
    </tbody>
  </table>
</div>
</div>