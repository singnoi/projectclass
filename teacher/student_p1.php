<div class="jumbotron">
    <div class="container">
  <h2>รายชื่อนักศึกษาโครงงาน 1</h2>          
  <table class="table">
    <thead class="bg-info">
      <tr>
      <th>รหัส</th>
      <th>ชื่อสกุล</th>
      <th>ชื่อเข้าใช้</th>
      <th>รหัสผ่าน</th>
      <th>เบอร์โทร</th>
      <th>อีเมล์</th>
      <th>ชื่อวิชา</th>
      <th>ภาคการเรียน</th>
      <th>ตัวดำเนินการ</th>
      
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>John</td>
        <td>Doe</td>
        <td>john@example.com</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>
        <a href="#" class="btn btn-warning btn-sm" > <i class="fa fa-edit"></i> </a>
        <a href="#" class="btn btn-danger btn-sm" > <i class="fa fa-trash"></i> </a>
        </td>
      </tr>
      <tr>
        <td>Mary</td>
        <td>Moe</td>
        <td>mary@example.com</td>
      </tr>
      <tr>
        <td>July</td>
        <td>Dooley</td>
        <td>july@example.com</td>
      </tr>
    </tbody>
  </table>
</div>

   
</div>