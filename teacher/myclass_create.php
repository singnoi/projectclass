<?php
include '../includes/config.php';
include '../includes/function.php';
$user_id = $_SESSION['user_id'];
?>
<div class="jumbotron">
<form action="myclass_action.php" action="POST" id="form_add">

<input type="hidden" name="user_id" value="<?php echo $user_id;?>" >
<input type="hidden" name="action" value="add" >

  <div class="form-group row">
    <label class="col-2" >ชื่อชั้นเรียน :</label>
    <div class="col-8">

  <select class="form-control" name="subject_list_id" required >
    <option value="" disabled selected hidden> เลือกรายวิชา </option>
    <?php $q1 = "SELECT * from tbl_subject_list";
          $r1 = $con->query($q1) or die ($q1);
          while ($ob1 = $r1->fetch_object()) {
            echo "<option value=\"{$ob1->subject_list_id}\"> {$ob1->subject_list_name} </option> ";
          }
    ?>
  </select>

  </div>
  </diV>
  <div class="form-group row">
    <label class="col-2" >ปีการศึกษา :</label>
    <div class="col-8"> 
    <select class="form-control" name="term_id" required >
          <option value="" disabled selected hidden > เลือกปีการศึกษา </option>
    <?php
    
    $q2 = "SELECT * from tbl_term";
    $r2 = $con->query($q2) or die ($q2);
    
    $n2 = $r2->num_rows;

    if( $n2 > 0 ) {
      while ($ob2 = $r2->fetch_object()) {
        echo "<option value=\"{$ob2->term_id}\" > ".sector_name($ob2->sector)."/".$ob2->year_name." </option>  ";
      }

    } else {
      echo "<option value=\"\" > ยังไม่มีข้อมูลปีการศึกษา </option>";
    }
    ?>
    </select>
    </div>
  </div>
  
  <a href="?page=myclass_p1" class="btn btn-secondary">ยกเลิก</a>
  <button type="submit" class="btn btn-info">บันทึก</button>
</div>
  
</form>
<script>
$('#form_add').submit(function(e){
  e.preventDefault();
  //alert("ddd");
  $.post("myclass_action.php",$('#form_add').serialize(),function(info){
    if(info=='ok') {
      window.location = '?page=myclass_p1';
    } else {
      alert(info);
    }
  });
});
</script>


</div>