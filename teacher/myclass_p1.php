<div class="row">
      <div class="col-md-12">
            <a href="?page=myclass_create" class="btn btn-success float-right" > <i class="fa fa-plus mr-3"></i> สร้างรายวิชาโครงงานของฉัน </a>
      </div>
</div><!-- row -->
<div class="row">
<?php
$user_id = $_SESSION['user_id'];
include '../includes/config.php';
include '../includes/function.php';
$q = "SELECT
*,
u.tname,
u.fname,
u.lname
FROM
tbl_subject_list AS l
JOIN tbl_subject AS s
ON l.subject_list_id = s.subject_list_id 
JOIN tbl_user as u
ON s.user_id = u.user_id 
JOIN tbl_term as t
ON s.term_id = t.term_id
where s.user_id='$user_id' ";

$r = $con->query($q) or die ($q);
$n = $r->num_rows;

if( $n > 0 ) {
      while ($ob = $r->fetch_object()) {
            # code...
      
?>
      <div class="col-md-12 mt-2">
            <div class="card" style="background-color: #ced9ed;" >
                  <div class="card-body">
                        <h1 class="display-5"><?php echo $ob->subject_list_name;?> </h1>
                        <p>
                        <?php echo " อาจารย์ : ".$ob->fname." ".$ob->lname." ".sector_name($ob->sector)."/".$ob->year_name." สาขาเทคโนโลยีสารสนเทศ ";?>

                        <span class='float-right'>
 
                        <a href="../../projectclass/teacher/?page=subject_data&subject_id=<?php echo $ob->subject_id;?>" class="btn btn-primary" role="button">เข้าสู่ชั้นเรียนที่ฉันสอน</a> 
                        &nbsp;
                        <button class="btn btn-danger" type="button" onclick="del('<?php echo $ob->subject_id;?>');" > <i class="fa fa-trash"></i> ลบ</button>
                        </span>
                        </p>
                  </div>
            </div>
      </div>

<?php
      }
} else {
?>
<div class="col-12 mt-2 text-center"><h4 class="text-center">ยังไม่มีรายวิชาที่ฉันรับผิดชอบ</h4></div>
<?php
}


?>
</div><!-- row -->
<script>
function del(id) {
      var ok = confirm("ต้องการลบรายการใช่หรือไม่");
      /*
      if(ok){
            $.post();
      }
      */
}
</script>
