<div class="jumbotron">
<div class="card">
    <div class="card-header bg-warning text-white">
       ตรรางรายชื่อคำร้องขออนุมัติโครงงาน
    </div>
    <div class="card-body">
    <table class="table table-bordered table-striped table-hover">
    <thead class="bg-info">
      <tr>
        <th>ลำดับ</th>
        <th>ชื่อคำร้อง</th>
        <th>ชื่อหัวข้อโครงงาน</th>
        <th>ชื่อผู้จัดทำ</th>
        <th>ดูรายละเอียด</th>
        <th>สถานะ</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td>PIT-01</td>
        <td>ระบบบริหารจัดการโครงงานหนึ่งและโครงงานสอง</td>
        <td>นายลีปาว ซ่ง . นางสาวศิริจรรยา ประโกทะสังข์</td>
        <td><a href="#" data-toggle="tooltip" title="Hooray!">คลิกดู</a></td>
        <td>อนุมัติ</td>
      </tr>
      <tr>
        <td>2</td>
        <td>PIT-01</td>
        <td>ระบบประกันคุณภาพ</td>
        <td>นางสาวพิมลศิริ ศรีทอง . นางสาวปาลิตา นิ่มน้อย</td>
        <td><a href="#" data-toggle="tooltip" title="Hooray!">คลิกดู</a></td>
        <td>ไม่อนุมัติแล้ว</td>
      </tr>
    </tbody>
  </table>
    </div>
</div>
</div>
