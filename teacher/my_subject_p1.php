<div class="jumbotron jumbotron-fluid">
<div class="container">

<div class="card">
            <div class="card-header bg-info">
            รายวิชาที่รับผิดชอบ
            </div>
            <div class="card-body">
                <table class="table table-light">
                    <thead class="thead-light">
                        <tr>
                            <th>ลำดับ</th><th>ปีการศึกษา</th><th>รายวิชา</th><th>อ.ผู้รับผิดชอบ</th><th>สถานะ</th><th class="text-center">เปิดคอร์ส</th>
                            <th class="text-center">รายละเอียด</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        include '../includes/config.php';
                        include '../includes/function.php';
                        $q = " SELECT
                        s.subject_id,
                        t.sector,
                        t.year_name,
                        l.subject_list_name,
                        u.tname, u.fname, u.lname,
                        st.sector_name ,
                        s.status 
                        FROM
                        tbl_subject as s 
                        JOIN tbl_subject_list as l
                        ON s.subject_list_id = l.subject_list_id 
                        JOIN tbl_user as u
                        ON s.user_id = u.user_id 
                        JOIN tbl_term as t
                        ON s.term_id = t.term_id
                        JOIN tbl_sector as st
                        ON st.sector = t.sector
                        where s.user_id = '$user_id'
                         "; 

                        $r = $con->query($q) or die ($q);
                        $n = $r->num_rows;
                        if( $n > 0 ) {
                            $i = 0;
                            while ($ob = $r->fetch_object()) {
                                $i++;
                                echo "<tr>";
                                    echo "<td>{$i}</td>";
                                    echo "<td>".$ob->sector_name."/".$ob->year_name."</td>";
                                    echo "<td>{$ob->subject_list_name}</td>";
                                    echo "<td>".$ob->tname.$ob->fname." ".$ob->lname."</td>";
                                    echo "<td>".show_status($ob->status)."</td>";
                                    echo "<td class=\"text-center\">";
                                    if($ob->status == '1') {
                                        ?>
                                        <a href="#" class="btn btn-warning" onclick="open_subject('<?php echo $ob->subject_id;?>');" > <i class="fas fa-key"></i> </a>
                                        <?php 
                                    } else {
                                        echo "<i class=\"fas fa-key text-secondary\"></i>";
                                    }
                                    echo "</td>";

                                    echo "<td class=\"text-center\">";
                                    if($ob->status == '2' || $ob->status == '3') {
                                        ?>
                                        <a href="?page=main&subject_id=<?php echo $ob->subject_id;?>" > <i class="fas fa-book fa-2x text-primary"></i> </a>
                                        <?php 
                                    } else {
                                        echo "<i class=\"fa fa-book text-secondary\"></i>";
                                    }
                                    echo "</td>";

                                echo "</tr>";
                            }
                        }
                        ?>
                        
                    </tbody>
                </table>
            </div>
        </div>





</div>
<script>
function open_subject(id) {
  var ok = confirm("ต้องการเปิดคอร์ส ใช่หรือไม่ ");
  if(ok) {
    $.post("my_subject_action.php",{id: id},function(info){
      if(info == 'ok'){
        window.location = '?page=my_subject_p1';
      } else {
        alert(info);
      }
    });
  }
}
</script>