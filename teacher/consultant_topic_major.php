<div class="jumbotron">
<div class="card">
    <div class="card-header bg-secondary text-white">
    รายชื่อหัวข้อโครงงานที่ขอเป็นที่ปรึกษาร่วม
    </div>
    <div class="card-body">
    <table class="table table-bordered table-striped table-hover">
    <thead class="bg-info">
      <tr>
        <th>ลำดับ</th>
        <th>ชื่อโครงงาน</th>
        <th>ชื่อผู้จัดทำ</th>
        <th>ปีการศึกษา</th>
        <th>สถานะการอนุมัติ</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td>ระบบบริหารจัดการโครงงานหนึ่งและโครงงานสอง</td>
        <td>นายลีปาว ซ่ง . นางสาวศิริจรรยา ประโกทะสังข์</td>
        <td>2561</td>
        <td>อนุมัติแล้ว</td>
      </tr>
      <tr>
        <td>2</td>
        <td>ระบบประกันคุณภาพ</td>
        <td>นางสาวพิมลศิริ ศรีทอง . นางสาวปาลิตา นิ่มน้อย</td>
        <td>2561</td>
        <td>ไม่อนุมัติแล้ว</td>
      </tr>
    </tbody>
  </table>
    </div>
</div>

<div class="card mt-5">
    <div class="card-header bg-success text-white">
    รายชื่อหัวข้อโครงงานที่อยู่ภายใต้การดูแลของฉัน (ที่ปรึกษาร่วม)
    </div>
    <div class="card-body">
    <table class="table table-bordered table-striped table-hover">
    <thead class="bg-info">
      <tr>
        <th>ลำดับ</th>
        <th>ชื่อโครงงาน</th>
        <th>ชื่อผู้จัดทำ</th>
        <th>ปีการศึกษา</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td>ระบบบริหารจัดการโครงงานหนึ่งและโครงงานสอง</td>
        <td>นายลีปาว ซ่ง . นางสาวศิริจรรยา ประโกทะสังข์</td>
        <td>2561</td>
       
      </tr>
    </tbody>
  </table>
    </div>
</div>
</div>
