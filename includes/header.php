  <style>
  .dropdown:hover>.dropdown-menu{
    display: block;
  }
  </style>

<!-- Image and text -->
<nav class="navbar fixed-top navbar-expand-sm navbar-dark bg-primary">
    <a class="navbar-brand" href="../../projectclass/" class="text-white">
      <img src="../../projectclass/4544.png_1200.png" width="30" height="30" class="d-inline-block align-top" alt="">
      โปรแกรมจัดการระบบรายวิชาโครงงาน
    </a>
    <a class="navbar-link text-white" href="../../projectclass/?page=main" ><i class="fas fa-home"></i> หน้าหลัก</a>

    <!-- Links -->
    <!-- <div class="btn-group">
        <button type="button" class="btn btn-outline-primary text-white dropdown-toggle" data-toggle="dropdown">
         ประกาศผลสอบ
        </button>
           <div class="dropdown-menu">
              <a class="dropdown-item" href="#">ผลการสอบหัวข้อโครงงาน</a>
              <a class="dropdown-item" href="#">ผลการสอบโครงงาน 1</a>
              <a class="dropdown-item" href="#">ผลการสอบโครงงาน 2</a>
          </div>
    </div> -->

    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          ประกาศผลสอบ 
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#"><i class="far fa-file-alt fa-xs"> </i> ผลการสอบหัวข้อโครงงาน</a>
          <a class="dropdown-item" href="#"><i class="far fa-file-alt fa-xs"> </i> ผลการสอบโครงงาน 1</a>
          <a class="dropdown-item" href="#"><i class="far fa-file-alt fa-xs"> </i> ผลการสอบโครงงาน 2</a>

          <!-- <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a> -->
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">ปฏิทิน</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">ข้อมูลโครงงานนักศึกษา</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">ประชาสัมพันธ์</a>
      </li>

  <?php 
    if($logined) { 
      if($_SESSION['admin']=='Y') {
        ?>
  <!-- Dropdown -->
  
  <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
        จัดการข้อมูลระบบ
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="../../projectclass/manage/">ข้อมูลอาจารย์</a>
        <a class="dropdown-item" href="../../projectclass/manage/?page=student_list">ข้อมูลนักศึกษา</a>
        <!-- <a class="dropdown-item" href="../../projectclass/manage?page=create_myclass">สร้างชั้นเรียน</a> -->
        <!-- <a class="dropdown-item" href="../../projectclass/manage?page=myclass_p1">ชั้นเรียนที่ฉันสอน</a> -->
        <a class="dropdown-item" href="../../projectclass/manage?page=year_form">ปีการศึกษา</a>
      </div>
    </li>
      <?php }
      if($_SESSION['user_type_id']=='2'){
        ?>
        
  <ul class="navbar-nav">
    <li class="nav-item">
     <!-- <a href="../projectclass">วิชาโครงการ</a> -->
      <a class="nav-link" href="../../projectclass/student/?page=student_page1">เมนูนักศึกษา</a>
    </li>
  </ul>


    <?php  }
    }
    ?>

</ul>


<form class="form-inline my-2 my-lg-0">
  <?php if($logined) { ?>
    <span class="text-light text-right">
    
    <a class="my-2 my-sm-0 text-light" href="../../projectclass/index.php?page=profile" data-toggle="tooltip" title="<?php echo $_SESSION['fullname']." (".$_SESSION['user_type_name'].")"; ?>" data-placement="bottom" > <i class="fas fa-user-circle fa-2x"></i></a>
    &nbsp;&nbsp;&nbsp;
    <a class="my-2 my-sm-0 text-light" href="../../projectclass/logout.php" data-toggle="tooltip" title="ออกจากระบบ" > <i class="fas fa-power-off fa-2x"></i></a>
    </span>
  <?php } else{
    ?>
    <span><a class="btn btn-outline-primary text-white" href="../../projectclass/?page=login" role="button"><i class="fas fa-sign-in-alt"></i> เข้าสู่ระบบ</a></span>
  <?php }
  
  ?>
  </form>

  <!-- <a class="navbar-brand" href="../../projectclass/?page=login" class="text-white"><i class="fas fa-sign-in-alt"></i> เข้าสู่ระบบ</a> -->
  <!-- <a class="btn btn-outline-primary text-white" href="../../projectclass/?page=login" role="button"><i class="fas fa-sign-in-alt"></i> เข้าสู่ระบบ</a> -->
</nav> 


<nav aria-label="breadcrumb" >
    <ol class="breadcrumb ">
      <li class="breadcrumb-item active" aria-current="page"><a href="../../projectclass/index.php"> หน้าหลัก</a></li>
      <?php if(isset($_GET['page'])) { ?>
      <li class="breadcrumb-item active" aria-current="page"><?php echo $page;?></li>
      <?php } ?>

    </ol>

</nav> 