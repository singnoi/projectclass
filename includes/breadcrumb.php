<nav aria-label="breadcrumb breadcrumb-fixed">
    <ol class="breadcrumb">
      <li class="breadcrumb-item active" aria-current="page"><a href="../../projectclass/index.php"> หน้าหลัก</a></li>
      <?php if(isset($_GET['page'])) { ?>
      <li class="breadcrumb-item active" aria-current="page"><?php echo $page;?></li>
      <?php } ?>

    </ol>

</nav> 

<style>
.breadcrumb-fixed {
  position: fixed;
  z-index: 2;
  width: 100%;
}

.breadcrumb-push {
  margin-top: 5em;
}
</style>