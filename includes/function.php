<?php 
function show_status($id){

    switch ($id) {
        case '1':
            $text = "<span class=\"badge badge-warning\" > <i class=\"far fa-clock\"></i> รอลงทะเบียน  </span> ";
            break;
        case '2':
            $text = "<span class=\"badge badge-success\" > <i class=\"fas fa-lock-open-alt\"></i> เปิดคอร์ส  </span> ";
            break;
        case '3':
            $text = "<span class=\"badge badge-secondary\" > <i class=\"fas fa-lock\"></i> ปิดคอร์สแล้ว  </span> ";
            break;
        
        default:
            # code...
            break;      
    } // switch

    return $text;
} // func

function show_type_user($id) {
    switch ($id) {
        case '1':
            $type_name = "อาจารย์";
            break;

        case '2':
            $type_name = "นักศึกษา";
            break;
        
        default:
        $type_name = "นักศึกษา";
            break;
    }
    return $type_name;
}

function sector_name($id) {
    switch ($id) {
        case '1':
            $type_name = "ภาคเรียนที่ 1";
            break;

        case '2':
            $type_name = "ภาคเรียนที่ 2";
            break;

        case '3':
            $type_name = "ภาคฤดูร้อน";
            break;
        
        default:
        $type_name = "ภาเรียนที่ 3";
            break;
    }
    return $type_name;
}

?>