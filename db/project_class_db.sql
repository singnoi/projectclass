/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100313
 Source Host           : localhost:3306
 Source Schema         : project_class_db

 Target Server Type    : MariaDB
 Target Server Version : 100313
 File Encoding         : 65001

 Date: 07/04/2019 12:55:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbl_sector
-- ----------------------------
DROP TABLE IF EXISTS `tbl_sector`;
CREATE TABLE `tbl_sector` (
  `sector` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `sector_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`sector`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbl_sector
-- ----------------------------
BEGIN;
INSERT INTO `tbl_sector` VALUES ('1', 'ภาคเรียนที่ 1');
INSERT INTO `tbl_sector` VALUES ('2', 'ภาคเรียนที่ 2');
INSERT INTO `tbl_sector` VALUES ('3', 'ภาคเรียนฤดูร้อน');
COMMIT;

-- ----------------------------
-- Table structure for tbl_subject
-- ----------------------------
DROP TABLE IF EXISTS `tbl_subject`;
CREATE TABLE `tbl_subject` (
  `subject_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_list_id` int(11) DEFAULT NULL,
  `term_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date_create` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`subject_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbl_subject
-- ----------------------------
BEGIN;
INSERT INTO `tbl_subject` VALUES (6, 2, 4, 1, '2019-04-06 12:56:55', '1');
INSERT INTO `tbl_subject` VALUES (7, 1, 4, 7, '2019-04-06 16:44:49', '2');
INSERT INTO `tbl_subject` VALUES (8, 1, 2, 7, '2019-04-06 17:16:16', '1');
COMMIT;

-- ----------------------------
-- Table structure for tbl_subject_list
-- ----------------------------
DROP TABLE IF EXISTS `tbl_subject_list`;
CREATE TABLE `tbl_subject_list` (
  `subject_list_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_list_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`subject_list_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbl_subject_list
-- ----------------------------
BEGIN;
INSERT INTO `tbl_subject_list` VALUES (1, 'วิชาโครงงาน 1');
INSERT INTO `tbl_subject_list` VALUES (2, 'วิชาโครงงาน 2');
COMMIT;

-- ----------------------------
-- Table structure for tbl_term
-- ----------------------------
DROP TABLE IF EXISTS `tbl_term`;
CREATE TABLE `tbl_term` (
  `term_id` int(11) NOT NULL AUTO_INCREMENT,
  `year_name` int(4) DEFAULT NULL,
  `sector` int(1) DEFAULT NULL,
  PRIMARY KEY (`term_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbl_term
-- ----------------------------
BEGIN;
INSERT INTO `tbl_term` VALUES (1, 2561, 1);
INSERT INTO `tbl_term` VALUES (2, 2561, 2);
INSERT INTO `tbl_term` VALUES (4, 2561, 3);
COMMIT;

-- ----------------------------
-- Table structure for tbl_user
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tname` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `lname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_tel` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `admin` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `reg_status` enum('Y','N') COLLATE utf8_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbl_user
-- ----------------------------
BEGIN;
INSERT INTO `tbl_user` VALUES (1, 'admin', 'admin', 'นาย', 'ลีปาว', 'ซ่ง', 'leepao@gmail.com', '0987654321', '6062210003', 1, 'Y', 'N');
INSERT INTO `tbl_user` VALUES (2, 'abc', '1234', 'นาย', 'รัขเดช', 'สิงห์น้อย', 'rachadej@gmail.com', '0918617832', '6062280001', 2, 'N', 'N');
INSERT INTO `tbl_user` VALUES (4, 'thongphan', '1234', 'นาย', 'thongphan', 'pariwat', 'thongphan@gmail.com', '0918617832', '600212', 1, 'N', 'N');
INSERT INTO `tbl_user` VALUES (6, 'user1', '1234', 'นางสาว', 'พิมลศิริ', 'สีทอง', 'sitong@g22.com', '0987654304', '6062210001', 2, 'N', 'N');
INSERT INTO `tbl_user` VALUES (7, 'pichai', 'pichai', 'นาย', 'พิชัย ', 'ระเวงวัน', 'pichai@gmail.com', '0812345671', '000001', 1, 'N', 'N');
INSERT INTO `tbl_user` VALUES (8, 'peck', 'peck', 'นาย', 'พันธวุท', 'จันทรมงคล', 'peck@gmail.com', '0812345672', '000002', 1, 'N', 'N');
COMMIT;

-- ----------------------------
-- Table structure for tbl_user_type
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_type`;
CREATE TABLE `tbl_user_type` (
  `user_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbl_user_type
-- ----------------------------
BEGIN;
INSERT INTO `tbl_user_type` VALUES (1, 'อาจารย์');
INSERT INTO `tbl_user_type` VALUES (2, 'นักศึกษา');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
