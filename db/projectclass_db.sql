/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80015
 Source Host           : localhost:3306
 Source Schema         : projectclass_db

 Target Server Type    : MySQL
 Target Server Version : 80015
 File Encoding         : 65001

 Date: 18/03/2019 18:42:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbl_user
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_password` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tname` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `lname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_tel` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `admin` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbl_user
-- ----------------------------
BEGIN;
INSERT INTO `tbl_user` VALUES (1, 'admin', 'admin', 'นาย', 'ลีปาว', 'ซ่ง', 'leepao@gmail.com', '0987654321', '6062210003', 1, 'Y');
INSERT INTO `tbl_user` VALUES (2, 'abc', '1234', 'นาย', 'รัขเดช', 'สิงห์น้อย', 'rachadej@gmail.com', '0918617832', '6062280001', 2, 'N');
INSERT INTO `tbl_user` VALUES (4, 'thongphan', '1234', 'นาย', 'thongphan', 'pariwat', 'thongphan@gmail.com', '0918617832', '600212', 1, 'N');
INSERT INTO `tbl_user` VALUES (6, 'user1', '1234', 'นางสาว', 'พิมลศิริ', 'สีทอง', 'sitong@g22.com', '0987654304', '6062210001', 2, 'N');
COMMIT;

-- ----------------------------
-- Table structure for tbl_user_type
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_type`;
CREATE TABLE `tbl_user_type` (
  `user_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbl_user_type
-- ----------------------------
BEGIN;
INSERT INTO `tbl_user_type` VALUES (1, 'อาจารย์');
INSERT INTO `tbl_user_type` VALUES (2, 'นักศึกษา');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
