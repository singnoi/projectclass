<div class="jumbotron">
   <div class="row">
   <div class="col-12">
   <form id="form_term" action="year_action.php" method="POST">
  <input type="hidden" name="action" value="add" >
  <div class="form-group row">
    <label class="col-2" for="email">ปีการศึกษา :</label>
    <div class="col-8">
    <input type="number" class="form-control" id="year_name" name="year_name" required >
  </div>
  </diV>
  <div class="form-group row">
    <label class="col-2" for="pwd">ภาคเรียน :</label>
    <div class="col-8"> 
        <div class="form-check-inline">
          <label class="form-check-label">
            <input type="radio" class="form-check-input" name="sector" value="1" checked required >ภาคเรียนที่ 1
          </label>
        </div>
        <div class="form-check-inline">
          <label class="form-check-label">
            <input type="radio" class="form-check-input" name="sector" value="2" required >ภาคเรียนที่ 2
          </label>
        </div>
        <div class="form-check-inline disabled">
          <label class="form-check-label">
            <input type="radio" class="form-check-input" name="sector" value="3" required >ภาคฤดูร้อน
          </label>
        </div>
    </div>
  </div>
  
  <button type="button" class="btn btn-secondary">ยกเลิก</button>
  <button type="submit" class="btn btn-primary">บันทึก</button>

  
</form>
   </div>
   </div>
   
   <hr class="my-4">

   <div class="row">
   <div class="col-12" id="year_show">
   <?php include 'year_show.php'; ?>
   </div>
   </div>
</div>

<script>
$('#form_term').submit(function(e){
  e.preventDefault();
  //alert("ss");
  $.post("year_action.php",$('#form_term').serialize(),function(info){
    if(info=='ok'){
      $('#year_show').load("year_show.php");
    } else {
      alert(info);
      //$('#year_show').html(info);
    }
    
  });
});

</script>
