<div class="container ">
  <h2>ตารางปีการศึกษา</h2>
              
  <table class="table table-bordered table-hover ">
    <thead class="bg-info">
      <tr>
        <th>ปีการศึกษา</th>
        <th>ภาคเรียน</th>
        <th>แก้ไข</th>
        <th>ลบ</th>
      </tr>
    </thead>
    <tbody>
<?php
include '../includes/config.php';
include '../includes/function.php';
$q = "SELECT * from tbl_term order by year_name DESC, sector ASC";
$r = $con->query($q) or die ($q);
$n = $r->num_rows;
if( $n > 0 ){
  while ($ob = $r->fetch_object()) {
    echo "<tr>";
    echo "<td>{$ob->year_name}</td>";
    echo "<td>".sector_name($ob->sector)."</td>";
    ?>
    <td class="text-center">
    <a href="?page=year_edit&term_id=<?php echo $ob->term_id;?>" > <i class="fa fa-edit text-warning"></i> </a>
    </td>
    <td>
    <a href="#" onclick="year_del('<?php echo $ob->term_id;?>');" > <i class="fa fa-trash text-danger"></i> </a>
    </td>
    <?php 
    echo "</tr>";
  }

}
?>

    </tbody>
  </table>
  </div>

<script>
function year_del(term_id) {
  var ok = confirm("ต้องการลบรายการใช่หรือไม่");
  if(ok){
    $.post("year_action.php",{term_id: term_id,action: "del"},function(info){
      $('#year_show').load("year_show.php");
    });
  }
}

</script>
