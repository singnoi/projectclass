<?php
include '../includes/config.php';
?>
<div class="jumbotron">
<div class="card">
    <div class="card-header bg-warning text-white">
       รายการรออนุมัติเข้าใช้งาน
    </div>
    <div class="card-body">
    <table class="table table-bordered table-striped table-hover">
    <thead class="bg-info">
      <tr>
        <th>รหัสนักศึกษา</th>
        <th>ชื่อ-สกุล</th>
        <th>ชื่อเข้าใช้</th>
        <th>รหัสผ่าน</th>
        <th>เบอร์โทร</th>
        <th>อีเมลล์</th>
        <th>อนุมัติเข้าใช้งาน</th>
      </tr>
    </thead>
    <tbody>
    <?php
                    $q = "SELECT * from tbl_user where user_type_id = '2' order by user_code DESC ";
                    $r = $con->query($q) or die ($q);
                    $n = $r->num_rows;
                    if($n >0) {
                        while ($obj = $r->fetch_object()) {
                          $user_id = $obj->user_id;
                          $onoff = $obj->reg_status;
                            echo "<tr>";
                            echo "<td>".$obj->user_code."</td>";
                            echo "<td>".$obj->tname.$obj->fname." ".$obj->lname."</td>";
                            echo "<td>".$obj->username."</td>";
                            echo "<td>".$obj->user_password."</td>";
                            echo "<td>".$obj->user_tel."</td>";
                            echo "<td>".$obj->user_email."</td>";
                             echo"<td>"; ?>
                             <span id="onoff_<?php echo $user_id;?>">
                             <a href="#" onclick="change_onoff('<?php echo $user_id;?>','<?php echo $onoff;?>');" >
                             <?php 
                              if($onoff=='N') {
                             ?>
                             <i class="fas fa-toggle-off text-secondary fa-2x"></i>
                              <?php } else { ?>
                               
                              <i class="fas fa-toggle-on text-success fa-2x"></i>
                                <?php
                              }
                              ?>
                              </a>
                              </span>
                          <?php
                            echo "</td>";
                            echo "</tr>";

                        }
                      }
                            ?>
                           
    
    </tbody>
  </table>
    </div>
</div>
</div>

<span id="show_error"></span>

<script>
function change_onoff(user_id,onoff) { 
 
  $.post("student_reg_action.php",{ 
    user_id: user_id, 
    onoff: onoff
    },function(inf){
      //$('#show_error').html(inf);
      window.location = '?page=student_reg';
  });

}
</script>