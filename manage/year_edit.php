<?php 
include '../includes/config.php';
$term_id = $_GET['term_id'];
$q = "SELECT * from tbl_term where term_id = '$term_id'";
$r = $con->query($q) or die ($q);
$ob = $r->fetch_object();
?>
<div class="jumbotron">
   <div class="row">
   <div class="col-12">
   <h4>แก้ไข</h4>
   <form id="form_term" action="year_action.php" method="POST">
  <input type="hidden" name="action" value="edit" >
  <input type="hidden" name="term_id" value="<?php echo $term_id;?>" >
  <div class="form-group row">
    <label class="col-2" for="email">ปีการศึกษา :</label>
    <div class="col-8">
    <input type="number" class="form-control" id="year_name" name="year_name" value="<?php echo $ob->year_name;?>" required >
  </div>
  </diV>
  <div class="form-group row">
    <label class="col-2" for="pwd">ภาคเรียน :</label>
    <div class="col-8"> 
        <div class="form-check-inline">
          <label class="form-check-label">
            <input type="radio" class="form-check-input" name="sector" value="1" <?php if($ob->sector == '1') echo "checked";?> required >ภาคเรียนที่ 1
          </label>
        </div>
        <div class="form-check-inline">
          <label class="form-check-label">
            <input type="radio" class="form-check-input" name="sector" value="2" <?php if($ob->sector == '2') echo "checked";?>  required >ภาคเรียนที่ 2
          </label>
        </div>
        <div class="form-check-inline disabled">
          <label class="form-check-label">
            <input type="radio" class="form-check-input" name="sector" value="3" <?php if($ob->sector == '3') echo "checked";?>  required >ภาคฤดูร้อน
          </label>
        </div>
    </div>
  </div>
  
  <a href="?page=year_form" class="btn btn-secondary">ยกเลิก</a>
  <button type="submit" class="btn btn-primary">บันทึก</button>

  
</form>
   </div>
   </div>
   
   <hr class="my-4">

   <div class="row">
   <div class="col-12" id="year_show">
 
   </div>
   </div>
</div>

<script>
$('#form_term').submit(function(e){
  e.preventDefault();
  //alert("ss");
  $.post("year_action.php",$('#form_term').serialize(),function(info){
    if(info=='ok'){
        window.location = '?page=year_form';
      //$('#year_show').load("year_show.php");
    } else {
      $('#year_show').html(info);
    }
    
  });
});

</script>
