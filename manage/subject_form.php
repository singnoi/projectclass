<?php
include '../includes/config.php';
include '../includes/function.php';

?>
<div class="row"></div>
    <div class="col-lg-12">

    <div class="card">
        <div class="card-header">
            เพิ่มรายวิชา
        </div>
        <div class="card-body">
            <form method="post" id="form_subject">
                <div class="row">
                    <div class="col-3">ปีการศึกษา</div>
                    <div class="col-9">
                    <select class="form-control" name="term_id" required >
                    <?php 
                    $qt = "SELECT                
                    tbl_term.term_id,                   
                    tbl_term.year_name,                  
                    tbl_sector.sector_name
                    FROM                   
                    tbl_term
                    JOIN 
                    tbl_sector
                    ON 
                    tbl_term.sector = tbl_sector.sector
                    order by tbl_term.year_name DESC, tbl_term.sector DESC 
                    ";
                    $rt = $con->query($qt);
                    $nt = $rt->num_rows;
                    if ( $nt > 0 ) {
                        while ($obt = $rt->fetch_object()) {
                            echo "<option value=\"{$obt->term_id}\" > ".$obt->sector_name."/".$obt->year_name." </option> ";
                        }
                    } 
                    ?>
                    </select>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-3">รายวิชา</div>
                    <div class="col-9">
                        <select class="form-control" name="subject_list_id" required >
                            <option value="1" >วิชาโครงงาน 1</option>
                            <option value="2" >วิชาโครงงาน 2</option>
                        </select>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-3">อ.ผู้รับผิดชอบ</div>
                    <div class="col-9">
                    <select class="form-control" name="user_id" required >
                    <?php 
                    $qt = "SELECT
                    u.user_id,
                    u.tname,
                    u.fname,
                    u.lname,
                    u.user_type_id
                    FROM
                    tbl_user as u
                    where u.user_type_id = '1'
                    ";
                    $rt = $con->query($qt);
                    $nt = $rt->num_rows;
                    if ( $nt > 0 ) {
                        while ($obt = $rt->fetch_object()) {
                            echo "<option value=\"{$obt->user_id}\" > ".$obt->tname.$obt->fname." ".$obt->lname." </option> ";
                        }
                    } 
                    ?>
                    </select>
                    </div>

                    <div class="col-12 mt-3 text-right">
                        <input type="submit" class="btn btn-primary" value="บันทึก" >
                    </div>
                </div>

            </form>
        </div>
    </div>
    
    </div>
    <div class="col-lg-12 mt-3">
        <div class="card">
            <div class="card-header bg-warning">
                ข้อมูลรายวิชาทั้งหมด
            </div>
            <div class="card-body">
                <table class="table table-light">
                    <thead class="thead-light">
                        <tr>
                            <th>ลำดับ</th><th>ปีการศึกษา</th><th>รายวิชา</th><th>อ.ผู้รับผิดชอบ</th><th>สถานะ</th><th>ลบ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        
                        $q = " SELECT
                        s.subject_id,
                        t.sector,
                        t.year_name,
                        l.subject_list_name,
                        u.tname, u.fname, u.lname,
                        st.sector_name ,
                        s.status 
                        FROM
                        tbl_subject as s 
                        JOIN tbl_subject_list as l
                        ON s.subject_list_id = l.subject_list_id 
                        JOIN tbl_user as u
                        ON s.user_id = u.user_id 
                        JOIN tbl_term as t
                        ON s.term_id = t.term_id
                        JOIN tbl_sector as st
                        ON st.sector = t.sector
                         "; 

                        $r = $con->query($q) or die ($q);
                        $n = $r->num_rows;
                        if( $n > 0 ) {
                            $i = 0;
                            while ($ob = $r->fetch_object()) {
                                $i++;
                                echo "<tr>";
                                    echo "<td>{$i}</td>";
                                    echo "<td>".$ob->sector_name."/".$ob->year_name."</td>";
                                    echo "<td>{$ob->subject_list_name}</td>";
                                    echo "<td>".$ob->tname.$ob->fname." ".$ob->lname."</td>";
                                    echo "<td>".show_status($ob->status)."</td>";
                                    echo "<td>";
                                    if($ob->status == '1') {
                                        ?>
                                        <a href="#" onclick="del_subject('<?php echo $ob->subject_id;?>');" > <i class="fa fa-trash text-danger"></i> </a>
                                        <?php 
                                    } else {
                                        echo "<i class=\"fa fa-trash text-secondary\"></i>";
                                    }
                                    echo "</td>";
                                echo "</tr>";
                            }
                        }
                        ?>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
$('#form_subject').submit(function(e){
    e.preventDefault();
    //alert("sss");
    $.post("subject_action",$('#form_subject').serialize(),function(info){
        if(info == 'ok') {
            window.location = '?page=subject_form';
        } else {
            alert(info);
        }
        
    });
});

function del_subject(id) { 
    var ok = confirm("ต้องการลบรายการ?");
        if(ok) {
            $.post("subject_action.php",{id: id, action: "del"},function(info){         
                window.location = '?page=subject_form';
            });
        }
 }
</script>