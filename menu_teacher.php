<div class="card">
            <div class="card-body p-0">
                <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-action bg-info"> <i class="fa fa-home mr-3" ></i>  หน้าหลักอาจารย์ </a>
                    <a href="../projectclass/teacher?page=consultant_topic_main" class="list-group-item list-group-item-action"> <i class="fas fa-server mr-3"></i>หัวข้อโครงงานที่ปรึกษาหลัก</a>
                    <a href="../projectclass/teacher?page=consultant_topic_major" class="list-group-item list-group-item-action"> <i class="fas fa-server mr-3"></i> หัวข้อโครงงานที่ปรึกษาร่วม</a>
                    <a href="../projectclass/teacher?page=petition_app" class="list-group-item list-group-item-action"> <i class="fas fa-file-alt mr-3"></i>คำร้องที่รอการอนุมัติ</a>
                    <a href="../projectclass/teacher?page=my_subject_p1" class="list-group-item list-group-item-action"> <i class="fas fa-server mr-3"></i>รายวิชาที่ฉันรับผิดชอบ</a>
                    
                </div>
            </div>
        </div>